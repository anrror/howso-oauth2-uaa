package com.howso.cloud.uaa.granter;

import com.howso.cloud.oauth2.token.IdCardAuthenticationToken;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 手机号密码授权模式
 *
 * @author jjb
 * @date 2020/7/11
 * <p>


 */
public class IdCardPwdGranter extends AbstractTokenGranter {
    private static final String GRANT_TYPE = "idCard_password";

    private final AuthenticationManager authenticationManager;

    public IdCardPwdGranter(AuthenticationManager authenticationManager, AuthorizationServerTokenServices tokenServices
            , ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory) {
        super(tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());
        String idCard = parameters.get("idCard");
        String password = parameters.get("password");
        parameters.remove("password");
        if(StringUtils.isAllEmpty(idCard,password)){
            throw new InvalidGrantException("the idCard and password can't be null" );
        }

        Authentication userAuth = new IdCardAuthenticationToken(idCard, password);
        ((AbstractAuthenticationToken) userAuth).setDetails(parameters);
        userAuth = authenticationManager.authenticate(userAuth);
        if (userAuth == null || !userAuth.isAuthenticated()) {
            throw new InvalidGrantException("Could not authenticate idCard: " + idCard);
        }

        OAuth2Request storedOAuth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
        return new OAuth2Authentication(storedOAuth2Request, userAuth);
    }
}
