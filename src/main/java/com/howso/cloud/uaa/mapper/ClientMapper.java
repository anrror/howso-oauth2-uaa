package com.howso.cloud.uaa.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.howso.cloud.db.mapper.SuperMapper;
import com.howso.cloud.uaa.model.Client;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface ClientMapper extends SuperMapper<Client> {
    List<Client> findList(Page<Client> page, @Param("params") Map<String, Object> params );
}
