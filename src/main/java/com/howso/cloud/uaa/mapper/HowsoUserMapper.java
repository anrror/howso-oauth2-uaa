package com.howso.cloud.uaa.mapper;

import com.howso.cloud.db.mapper.SuperMapper;
import com.howso.cloud.uaa.model.HowsoMenu;
import com.howso.cloud.uaa.model.HowsoUser;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;


/**
 * 放入用户和菜单 这里直接全部菜单给用户权限
 */
@Mapper
public interface HowsoUserMapper extends SuperMapper<HowsoUser> {

    List<HowsoMenu> findMenus();
}
