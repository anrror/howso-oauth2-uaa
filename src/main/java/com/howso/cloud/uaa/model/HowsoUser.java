package com.howso.cloud.uaa.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.howso.cloud.db.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = false)
@TableName("howso_user")
public class HowsoUser extends SuperEntity {

    private String username;
    private String password;
    private Boolean enabled;

    @TableLogic
    private boolean isDel;
}
