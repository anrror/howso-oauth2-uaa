package com.howso.cloud.uaa.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.howso.cloud.db.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;



@Data
@EqualsAndHashCode(callSuper = false)
@TableName("howso_menu")
public class HowsoMenu extends SuperEntity {

	private Long parentId;
	private String name;
	private String url;
	private Integer sort;
	/**
	 * 请求的类型
	 */
	private String pathMethod;
	private String clientId;
}
