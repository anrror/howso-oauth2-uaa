package com.howso.cloud.uaa.controller;

import com.howso.cloud.oauth2.model.SysMenu;
import com.howso.cloud.uaa.service.IHowsoUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HowsoUserController {
    @Autowired
    private IHowsoUserService howsoUserService;
    @GetMapping(value = "/menus")
    public List<SysMenu> findMenus(){
        List<SysMenu> list=new ArrayList<>();
        howsoUserService.findMenus().forEach(e->{
            SysMenu sysMenu=new SysMenu();
            BeanUtils.copyProperties(e,sysMenu);
            list.add(sysMenu);
        });
        return list;
    }
}
