package com.howso.cloud.uaa.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.howso.cloud.db.service.impl.SuperServiceImpl;
import com.howso.cloud.oauth2.model.LoginAppUser;
import com.howso.cloud.oauth2.model.SysRole;
import com.howso.cloud.oauth2.model.SysUser;
import com.howso.cloud.uaa.mapper.HowsoUserMapper;
import com.howso.cloud.uaa.model.HowsoMenu;
import com.howso.cloud.uaa.model.HowsoUser;
import com.howso.cloud.uaa.service.IHowsoUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class HowsoUserServiceImpl extends SuperServiceImpl<HowsoUserMapper, HowsoUser> implements IHowsoUserService {
    private final String CODES_STRING="HOWSO";

    @Override
    public LoginAppUser findByUsername(String username) {
        SysUser user=this.selectByUsername(username);
        return getLoginAppUser(user);
    }

    @Override
    public List<HowsoMenu> findMenus() {
        return baseMapper.findMenus();
    }

    public LoginAppUser getLoginAppUser(SysUser sysUser) {
        if (sysUser != null) {
            LoginAppUser loginAppUser = new LoginAppUser();
            try {
                BeanUtils.copyProperties(loginAppUser,sysUser);
                //启动无法copy？
                loginAppUser.setEnabled(sysUser.getEnabled());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
            List<SysRole> sysRoles=new ArrayList<>();
            SysRole sysRole=new SysRole();
            sysRole.setId(0L);
            sysRole.setCode(CODES_STRING);
            sysRole.setName("超管");
            // 设置角色
            loginAppUser.setRoles(sysRoles);
            List<HowsoMenu> menus=baseMapper.findMenus();
            if (!CollectionUtils.isEmpty(menus)) {
                Set<String> permissions = menus.stream().map(p -> p.getUrl())
                        .collect(Collectors.toSet());
                // 设置权限集合
                loginAppUser.setPermissions(permissions);
            }
            return loginAppUser;
        }
        return null;
    }

    public SysUser selectByUsername(String username) {
        List<HowsoUser> users = baseMapper.selectList(
                new QueryWrapper<HowsoUser>().eq("username", username)
        );
        return getUser(users);
    }


    private SysUser getUser(List<HowsoUser> users)  {
        SysUser user = new SysUser();
        if (users != null && !users.isEmpty()) {
            try {
                BeanUtils.copyProperties(user,users.get(0));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
}
