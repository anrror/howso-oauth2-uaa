package com.howso.cloud.uaa.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;

public interface HowsoUserDetailsService extends UserDetailsService {


    /**
     * 根据电话号码查询用户
     *
     * @param mobile
     * @return
     */
    UserDetails loadUserByMobile(String mobile);
    /**
     * 根据email查询用户
     *
     * @param email
     * @return
     */
    UserDetails loadUserByEmail(String email);
    /**
     * 根据idCard查询用户
     *
     * @param idCard
     * @return
     */
    UserDetails loadUserByIdCard(String idCard);

    /**
     * 根据用户id/openId查询用户
     * @param userId 用户id/openId
     */
    SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException;
}
