package com.howso.cloud.uaa.service.impl;

import com.howso.cloud.uaa.service.HowsoUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 用户service工厂
 *
 */
@Slf4j
@Service
public class UserDetailServiceFactory {

    @Resource
    private HowsoUserDetailsService userDetailsService;

    public HowsoUserDetailsService getService() {
        return userDetailsService;
    }

}
