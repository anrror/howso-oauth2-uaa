package com.howso.cloud.uaa.service;



import com.howso.cloud.db.service.ISuperService;
import com.howso.cloud.oauth2.model.LoginAppUser;
import com.howso.cloud.uaa.model.HowsoMenu;
import com.howso.cloud.uaa.model.HowsoUser;

import java.util.List;

public interface IHowsoUserService  extends ISuperService<HowsoUser>{
	/**
	 * 获取UserDetails对象
	 * @param username
	 * @return
	 */
	LoginAppUser findByUsername(String username);

	List<HowsoMenu> findMenus();

}
