package com.howso.cloud.uaa.service;

import com.howso.cloud.db.service.ISuperService;
import com.howso.cloud.result.PageResult;
import com.howso.cloud.result.R;
import com.howso.cloud.uaa.model.Client;

import java.util.Map;


public interface IClientService extends ISuperService<Client> {
    R saveClient(Client clientDto) throws Exception;

    /**
     * 查询应用列表
     * @param params
     * @param isPage 是否分页
     */
    PageResult<Client> listClient(Map<String, Object> params, boolean isPage);

    void delClient(long id);

    Client loadClientByClientId(String clientId);
}
