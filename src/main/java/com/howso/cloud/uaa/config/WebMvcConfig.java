package com.howso.cloud.uaa.config;

import com.howso.cloud.config.DefaultWebMvcConfig;
import org.springframework.context.annotation.Configuration;


@Configuration
public class WebMvcConfig extends DefaultWebMvcConfig {
}
