package com.howso.cloud.uaa.authentication.sms;


import com.howso.cloud.uaa.service.IValidateCodeService;
import com.howso.cloud.uaa.service.impl.UserDetailServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.stereotype.Component;

@Component
public class MobileSmsAuthenticationSecurityConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
    @Autowired
    private UserDetailServiceFactory userDetailsServiceFactory;

    @Autowired
    private IValidateCodeService validateCodeService;

    @Override
    public void configure(HttpSecurity http) {
        //mobile provider
        MobileSmsAuthenticationProvider provider = new MobileSmsAuthenticationProvider();
        provider.setUserDetailsService(userDetailsServiceFactory);
        provider.setValidateCodeService(validateCodeService);
        http.authenticationProvider(provider);
    }
}
