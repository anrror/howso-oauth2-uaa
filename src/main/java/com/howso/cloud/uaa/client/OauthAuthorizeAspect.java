package com.howso.cloud.uaa.client;


import com.howso.cloud.lb.context.ClientContextHolder;
import com.howso.cloud.oauth2.model.LoginAppUser;
import com.howso.cloud.oauth2.token.ClientUsernamePasswordAuthenticationToken;
import com.howso.cloud.uaa.service.impl.UserDetailServiceFactory;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.stereotype.Component;

import java.security.Principal;
import java.util.Map;

/**
 * /oauth/authorize拦截器
 * 解决不同租户单点登录时角色没变化
 *
 */
@Slf4j
@Component
@Aspect
public class OauthAuthorizeAspect {
    private final UserDetailServiceFactory userDetailsServiceFactory;

    public OauthAuthorizeAspect(UserDetailServiceFactory userDetailsServiceFactory) {
        this.userDetailsServiceFactory = userDetailsServiceFactory;
    }

    @Around("execution(* org.springframework.security.oauth2.provider.endpoint.AuthorizationEndpoint.authorize(..))")
    public Object doAroundMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        Map<String, String> parameters = (Map<String, String>) args[1];
        Principal principal = (Principal) args[3];
        if (principal instanceof ClientUsernamePasswordAuthenticationToken) {
            ClientUsernamePasswordAuthenticationToken clientToken = (ClientUsernamePasswordAuthenticationToken)principal;
            String clientId = clientToken.getClientId();
            String requestClientId = parameters.get(OAuth2Utils.CLIENT_ID);
            //判断是否不同应用单点登录
            if (!requestClientId.equals(clientId)) {
                Object details = clientToken.getDetails();
                try {
                    ClientContextHolder.setClient(requestClientId);
                    //重新查询对应该租户的角色等信息
                    LoginAppUser user = (LoginAppUser)userDetailsServiceFactory.getService()
                            .loadUserByUsername(clientToken.getName());
                    clientToken = new ClientUsernamePasswordAuthenticationToken(user, clientToken.getCredentials(), user.getAuthorities(), requestClientId);
                    clientToken.setDetails(details);
                    args[3] = clientToken;
                } finally {
                    ClientContextHolder.clear();
                }
            }
        }
        return joinPoint.proceed(args);
    }
}
