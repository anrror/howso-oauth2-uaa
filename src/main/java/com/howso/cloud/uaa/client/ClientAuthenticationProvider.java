package com.howso.cloud.uaa.client;

import com.howso.cloud.oauth2.token.ClientUsernamePasswordAuthenticationToken;
import com.howso.cloud.uaa.authentication.password.PasswordAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 增加租户id，解决不同租户单点登录时角色没变化
 *

 */
public class ClientAuthenticationProvider extends PasswordAuthenticationProvider {
    @Override
    protected Authentication createSuccessAuthentication(Object principal,
                                                         Authentication authentication, UserDetails user) {
        ClientUsernamePasswordAuthenticationToken authenticationToken = (ClientUsernamePasswordAuthenticationToken) authentication;
        ClientUsernamePasswordAuthenticationToken result = new ClientUsernamePasswordAuthenticationToken(
                principal, authentication.getCredentials(), user.getAuthorities(), authenticationToken.getClientId());
        result.setDetails(authenticationToken.getDetails());
        return result;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return ClientUsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
